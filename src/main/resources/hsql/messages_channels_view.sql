CREATE VIEW ChannelMessages AS
    SELECT time, author, message, UNIX_TIMESTAMP(time) AS timestamp, Channels.name AS channelName, Channels.id AS channelId
    FROM Messages LEFT JOIN Channels ON (Messages.target = Channels.id);