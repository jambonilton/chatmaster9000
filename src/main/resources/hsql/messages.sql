CREATE TABLE Messages (
    time timestamp DEFAULT CURRENT_TIMESTAMP,
    target varchar(36),
    author varchar(128) NOT NULL,
    message longvarchar NOT NULL
)