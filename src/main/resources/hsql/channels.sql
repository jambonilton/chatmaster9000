CREATE TABLE Channels (
    id varchar(36) NOT NULL,
    name varchar(128) NOT NULL,
    privacy smallint NOT NULL
)