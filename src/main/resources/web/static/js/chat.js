

/*
 * CHAT WINDOW STATE
 */
var chat = {
    user: {
        get: function() {
            return cookies.get('username');
        },
        set: function(value) {
            cookies.set('username', value, 999);
        }
    },
    channel: {
        get: function() {
            var path = window.location.pathname;
            if (path.startsWith('/c/')) {
                return path.substring(3);
            }
            return null;
        }
    }
};


/*
 * STARTUP
 */
var usernameElement = document.getElementById('username');
var channelElement = document.getElementById('channel');
var channelsElement = document.getElementById('channels');
var chatElement = document.getElementById('chat');
var chatInputElement = document.getElementById('chat-input');

window.onload = function() {
    if (chat.user.get()) {
        console.log('Welcome back, '+chat.user.get());
        usernameElement.value = chat.user.get();
    }
    if (!chat.channel.get()) {
        channelElement.innerHTML = 'lobby';
    }
    // load messages
    messages.list({target:chat.channel.get(), limit:40}, function(messages) {
        var html = '';
        for (var i in messages) {
            html += messageTemplate(messages[i]);
        }
        chatElement.innerHTML = chatElement.innerHTML + html;
        scrollToBottom();
    });
    // populate channel
    channels.get(chat.channel.get(), function(c) {
        channelElement.innerHTML = c.name;
    })
    // load channels list
    channels.list(function(channels) {
        var channelsHtml = '';
        for (var i in channels) {
            channelsHtml += '<a href="/c/'+channels[i].id+'">'+channels[i].name+'</a>';
        }
        channelsElement.innerHTML = '<div id="channels-links">' + channelsHtml + '</div>'
            + '<div class="divider"></div><div id="new-channel-link" onclick="toggleNewChannel()">* new *</div>';
    });
    // chat text input events
    chatInputElement.onkeyup = function(event) {
        if (event.keyCode==13 && messages.send(chat.user.get(), chat.channel.get(), chatInputElement.value)) {
            chatInputElement.value='';
        }
    }
}

// hold active state for sending notifications
var windowIsActive = true;
window.onfocus = function () { windowIsActive = true; };
window.onblur = function () { windowIsActive = false; };

sockets.open('/chat', function(event){showMessage(JSON.parse(event.data));});


/*
 * MISC
 */
function toggleChannels() {
    var element = document.getElementById('channels');
    if (element.style.left === '-100%') {
        element.style.left = '0';
    } else {
        element.style.left = '-100%';
    }
}
function toggleNewChannel() {
    var display = document.getElementById('new-channel-modal-form').style.display;
    if (display == 'block') {
        document.getElementById('new-channel-modal-backdrop').style.display = 'none';
        document.getElementById('new-channel-modal-form').style.display = 'none';
    } else {
        document.getElementById('new-channel-modal-backdrop').style.display = 'block';
        document.getElementById('new-channel-modal-form').style.display = 'block';
    }
}
function scrollToBottom() {
    setTimeout(function() {
        var chatMain = document.getElementById('chat-main');
        chatMain.scrollTop = chatMain.scrollHeight;
    }, 500);
}
function showMessage(messageData, suppressNotification) {
     if (messageData.target != chat.channel.get()) {
         return;
     }
     var shouldScroll = chatElement.scrollTop === (chatElement.scrollHeight - chatElement.offsetHeight);

     var text = messageData.message.replace(/(https?:\/\/\S+)/gi, function(link) {
         return /.gif|.jpg|.jpeg|.svg|.png/.test(link) ? '<img src="'+link+'" />' : '<a href="'+link+'">'+link+'</a>';
     });

     chatElement.innerHTML = chatElement.innerHTML + messageTemplate(messageData);

     if (shouldScroll) {
         scrollToBottom();
     }
     if (!suppressNotification) {
         showNotification(messageData);
     }
 }
 function messageTemplate(messageData) {
     var text = messageData.message.replace(/(https?:\/\/\S+)/gi, function(link) {
         return /.gif|.jpg|.jpeg|.svg|.png/.test(link)
            ? '<div class="image-wrapper"><img src="'+link+'" />&nbsp;'
                +'<a href="javascript:void(0)" onclick="this.parentNode.style.display=\'none\'">hide</a></div>'
            : '<a href="'+link+'">'+link+'</a>';
     });
     return '<article><div><aside class="username">' + messageData.author + '</aside>'
         + '<time>' + formatTime(messageData.time) + '</time>'
         + '<main>' + text + '</main></div></article>';
 }
 function showNotification(messageData) {

    var showMessageNotification = function(permission) {
        if (windowIsActive) {
            return;
        } else if (permission === "granted") {
            var notification = new Notification('Message from '+messageData.author, {body: messageData.message});
        } else if (permission !== 'denied') {
            Notification.requestPermission(showMessageNotification);
        }
    };

    showMessageNotification(Notification.permission);

}
function formatTime(timestamp) {
    var date = new Date(timestamp);
    return isToday(timestamp)
        ? date.toLocaleTimeString()
        : date.toLocaleTimeString() + " " + date.toLocaleDateString("en-gb");
}
function isToday(timestamp) {
    return new Date(timestamp).setHours(0,0,0,0) == new Date().setHours(0,0,0,0)
}