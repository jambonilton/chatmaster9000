/*
 * MESSAGES
 */
var messages = {
    send: function(author, channel, content) {
        if (!content || !content.trim()) {
            return true;
        }
        return sockets.send(JSON.stringify({
            message: content.trim(),
            target: channel,
            author: author
        }));
    },
    list: function(query, callback) {
        http.get({ url: '/messages', params:query }, callback);
    }
};