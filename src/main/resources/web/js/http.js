var http = {
    get: function(request, callback) {
        var url;
        if (typeof request === 'string') {
            url = request;
        } else if (typeof request === 'object') {
            url = request.url;
            if (request.params) {
                url += '?';
                for (var key in request.params) {
                    if (request.params[key]) {
                        url += key + "=" + request.params[key] + '&';
                    }
                }
                url = url.substring(0, url.length-1);
            }
        }
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", url, true);
        if (request.headers) {
            for (key in request.headers) {
                xhttp.setRequestHeader(key, request.headers[key]);
            }
        }
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200 && callback) {
                callback(JSON.parse(xhttp.responseText));
            }
        };
        xhttp.send();
    }
}