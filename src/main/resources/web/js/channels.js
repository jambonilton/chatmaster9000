/*
 * CHANNELS
 */
var channels = {
	get: function(id, callback) {
	    if (!id) {
	        callback({id:id, name:'lobby'})
	    } else {
	        http.get('/channels/'+id, callback);
	    }
	},
	list: function(callback) {
		http.get('/channels', callback);
	}
};