
/*
 *  SOCKETS
 */
var sockets = {

    open: function(path, onmessage) {
        if (!window.WebSocket) {
            alert("Your browser does not support Websockets. GTFO.");
        }
        console.log('Connecting to socket '+window.location.host+path);
        if (window.location.protocol === 'https:') {
            this.socket = new WebSocket('wss://'+window.location.host+path);
        } else {
            this.socket = new WebSocket('ws://'+window.location.host+path);
        }
        this.socket.onmessage = this.socket.onmessage || onmessage;
        this.socket.onclose = this.socket.onclose || this.open.bind(this, path, onmessage); // re-open socket when it closes
    },

    send: function(str) {
        if (!window.WebSocket
            || this.socket.readyState != WebSocket.OPEN) {
            return false;
        }
        this.socket.send(str);
        return true;
    }

};