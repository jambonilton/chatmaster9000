package org.nps.cm.messages;

import org.nps.cm.sql.DAO;

public interface MessagesDAO extends DAO<Message, MessagesQuery> {

}
