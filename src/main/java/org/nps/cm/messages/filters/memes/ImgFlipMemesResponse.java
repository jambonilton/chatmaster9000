package org.nps.cm.messages.filters.memes;

import java.util.List;

public class ImgFlipMemesResponse {

    boolean success;
    ImgFlipMemesResponseData data;

    public boolean isSuccess() {
        return success;
    }

    public ImgFlipMemesResponse setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public ImgFlipMemesResponseData getData() {
        return data;
    }

    public ImgFlipMemesResponse setData(ImgFlipMemesResponseData data) {
        this.data = data;
        return this;
    }

    public static class ImgFlipMemesResponseData {

        List<ImgFlipMeme> memes;

        public List<ImgFlipMeme> getMemes() {
            return memes;
        }

        public ImgFlipMemesResponseData setMemes(List<ImgFlipMeme> memes) {
            this.memes = memes;
            return this;
        }
    }

}
