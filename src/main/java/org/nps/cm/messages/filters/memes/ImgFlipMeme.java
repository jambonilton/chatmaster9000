package org.nps.cm.messages.filters.memes;

public class ImgFlipMeme {

    Long id;
    String name;
    String url;
    Integer width;
    Integer height;

    public Long getId() {
        return id;
    }

    public ImgFlipMeme setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public ImgFlipMeme setName(String name) {
        this.name = name;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public ImgFlipMeme setUrl(String url) {
        this.url = url;
        return this;
    }

    public Integer getWidth() {
        return width;
    }

    public ImgFlipMeme setWidth(Integer width) {
        this.width = width;
        return this;
    }

    public Integer getHeight() {
        return height;
    }

    public ImgFlipMeme setHeight(Integer height) {
        this.height = height;
        return this;
    }
}
