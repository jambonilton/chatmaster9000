package org.nps.cm.messages.filters.gifs;

import com.google.gson.Gson;
import org.nps.cm.http.client.HttpClient;
import org.nps.cm.http.client.HttpRequest;
import org.nps.cm.http.client.HttpResponse;
import org.nps.cm.io.ByteStreams;
import org.nps.cm.messages.Message;
import org.nps.cm.messages.filters.MessageFilter;

import javax.ws.rs.core.UriBuilder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Calls out to gifs translate endpoint to convert messages like "gif tacos" with an animated gif.
 */
public class GiphyGifTranslator implements MessageFilter {

    static final Pattern GIF_PATTERN = Pattern.compile("\\s*gif\\s+(.+)");
    static final String GIPHY_TRANSLATE_URL = "http://api.giphy.com/v1/gifs/translate";
    static final String GIPHY_API_KEY = "dc6zaTOxFJmzC";

    final HttpClient client;
    final Gson gson;

    public GiphyGifTranslator(HttpClient client, Gson gson) {
        this.client = client;
        this.gson = gson;
    }

    @Override
    public Message apply(Message message) {
        final Matcher matcher = GIF_PATTERN.matcher(message.getMessage());
        if (matcher.matches()) {
            final String gifSearch = matcher.group(1);
            final HttpRequest request = new HttpRequest(UriBuilder.fromUri(GIPHY_TRANSLATE_URL)
                    .queryParam("s", gifSearch)
                    .queryParam("api_key", GIPHY_API_KEY)
                    .build());
            final HttpResponse response = client.send(request);
            final String json = ByteStreams.toString(response.getInputStream());
            final GiphyTranslateResponse giphyResponse = gson.fromJson(json, GiphyTranslateResponse.class);
            final String gifUrl = giphyResponse.getData().getImages().getFixedHeight().getUrl();

            return new Message(message.getTime(), message.getTarget(), message.getAuthor(), gifUrl);
        }
        return message;
    }

}
