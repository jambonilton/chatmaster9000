package org.nps.cm.messages.filters;

import org.nps.cm.messages.Message;

import java.util.function.UnaryOperator;

public interface MessageFilter extends UnaryOperator<Message> {

}