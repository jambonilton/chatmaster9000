package org.nps.cm.messages.filters.memes;

import com.google.gson.Gson;
import org.nps.cm.http.client.HttpClient;
import org.nps.cm.http.client.HttpRequest;
import org.nps.cm.http.client.HttpResponse;
import org.nps.cm.log.Logger;
import org.nps.cm.messages.Message;
import org.nps.cm.messages.filters.MessageFilter;
import org.nps.cm.util.Lookup;
import org.nps.cm.util.string.SimpleCharacterSimilarity;
import org.nps.cm.util.string.WordMatchCountSimilarity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ImgflipMemesFilter implements MessageFilter {

    static final Pattern MEMES_PATTERN = Pattern.compile("\\s*memes?\\s+(.+)");
    static final String IMGFLIP_URL = "https://api.imgflip.com/caption_image";
    static final String IMGFLIP_ACCOUNT = "chatmaster";
    static final String IMGFLIP_PWD = "chatmaster9000";
    static final ImgFlipMeme BAD_LUCK = new ImgFlipMeme()
            .setId(61585L)
            .setName("Bad Luck Brian")
            .setUrl("https://i.imgflip.com/1bip.jpg")
            .setWidth(475)
            .setHeight(562);

    final HttpClient client;
    final Gson gson;
    final Lookup<String, ImgFlipMeme> memeSearch;

    public ImgflipMemesFilter(Logger logger, HttpClient client, Gson gson) {
        this.client = client;
        this.gson = gson;
        this.memeSearch = new ImgFlipMemeLookup(logger, client, gson, new WordMatchCountSimilarity(1, new SimpleCharacterSimilarity(2)));
    }

    @Override
    public Message apply(Message message) {
        final Matcher matcher = MEMES_PATTERN.matcher(message.getMessage());
        if (matcher.matches()) {
            final String memeString = matcher.group(1);
            final String[] memeStringParts = memeString.split(";");
            final ImgFlipMeme meme = memeStringParts.length > 0
                    ? memeSearch.find(memeStringParts[0]).orElse(BAD_LUCK)
                    : BAD_LUCK;
            final ImgFlipCaptionRequest body = new ImgFlipCaptionRequest()
                    .setTemplate_id(meme.getId())
                    .setUsername(IMGFLIP_ACCOUNT)
                    .setPassword(IMGFLIP_PWD)
                    .setText0(memeStringParts.length > 1 ? memeStringParts[1].trim() : null)
                    .setText1(memeStringParts.length > 2 ? memeStringParts[2].trim() : null);
            final HttpResponse response = client.send(new HttpRequest(IMGFLIP_URL)
                    .setMethod("POST")
                    .setBody(body));
            final ImgFlipCaptionResponse captionResponse = gson.fromJson(response.getReader(), ImgFlipCaptionResponse.class);
            final String url = captionResponse.isSuccess() ? captionResponse.getData().getUrl() : BAD_LUCK.getUrl();

            return new Message(message.getTime(), message.getTarget(), message.getAuthor(), url);
        }
        return message;
    }

}
