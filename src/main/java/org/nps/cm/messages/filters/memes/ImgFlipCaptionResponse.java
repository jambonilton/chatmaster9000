package org.nps.cm.messages.filters.memes;

public class ImgFlipCaptionResponse {

    boolean success;
    ImgFlipCaptionResponseData data;

    public boolean isSuccess() {
        return success;
    }

    public ImgFlipCaptionResponse setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public ImgFlipCaptionResponseData getData() {
        return data;
    }

    public ImgFlipCaptionResponse setData(ImgFlipCaptionResponseData data) {
        this.data = data;
        return this;
    }

    public static class ImgFlipCaptionResponseData {
        String url;
        String page_url;

        public String getUrl() {
            return url;
        }

        public ImgFlipCaptionResponseData setUrl(String url) {
            this.url = url;
            return this;
        }

        public String getPage_url() {
            return page_url;
        }

        public ImgFlipCaptionResponseData setPage_url(String page_url) {
            this.page_url = page_url;
            return this;
        }

    }

}
