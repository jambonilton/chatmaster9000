package org.nps.cm.messages.filters.memes;

public class ImgFlipCaptionRequest {

    Long template_id;
    String username;
    String password;
    String text0;
    String text1;

    public Long getTemplate_id() {
        return template_id;
    }

    public ImgFlipCaptionRequest setTemplate_id(Long template_id) {
        this.template_id = template_id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public ImgFlipCaptionRequest setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public ImgFlipCaptionRequest setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getText0() {
        return text0;
    }

    public ImgFlipCaptionRequest setText0(String text0) {
        this.text0 = text0;
        return this;
    }

    public String getText1() {
        return text1;
    }

    public ImgFlipCaptionRequest setText1(String text1) {
        this.text1 = text1;
        return this;
    }
}
