package org.nps.cm.messages.filters.memes;

import com.google.gson.Gson;
import org.nps.cm.http.client.HttpClient;
import org.nps.cm.http.client.HttpRequest;
import org.nps.cm.http.client.HttpResponse;
import org.nps.cm.io.ByteStreams;
import org.nps.cm.log.Logger;
import org.nps.cm.util.Comparators;
import org.nps.cm.util.Lookup;
import org.nps.cm.util.string.StringSimilarity;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class ImgFlipMemeLookup implements Lookup<String, ImgFlipMeme> {

    static final String MEMES_URL = "https://api.imgflip.com/get_memes";

    final Logger logger;
    final HttpClient httpClient;
    final Gson gson;
    final StringSimilarity stringSimilarity;

    List<ImgFlipMeme> memes;

    public ImgFlipMemeLookup(Logger logger, HttpClient httpClient, Gson gson, StringSimilarity stringSimilarity) {
        this.logger = logger;
        this.httpClient = httpClient;
        this.gson = gson;
        this.stringSimilarity = stringSimilarity;
    }

    @Override
    public Optional<ImgFlipMeme> find(String searchString) {
        final Optional<ScoredMeme> candidateMeme = getMemes().stream()
                .map(meme -> score(searchString, meme))
                .max(Comparators.mapping(ScoredMeme::getScore));
        candidateMeme.ifPresent(meme -> logger.log("Found meme \""+meme.getName()+"\" with search score "+meme.getScore()));
        return candidateMeme.map(ScoredMeme::getMeme);
    }

    private ScoredMeme score(String searchString, ImgFlipMeme meme) {
        return new ScoredMeme(100d*stringSimilarity.test(searchString, meme.getName()), meme);
    }

    public List<ImgFlipMeme> getMemes() {
        return memes == null ? memes = fetchMemes() : memes;
    }

    private List<ImgFlipMeme> fetchMemes() {
        final HttpResponse response = httpClient.send(new HttpRequest(MEMES_URL));
        final String json = ByteStreams.toString(response.getInputStream());
        final ImgFlipMemesResponse memes = gson.fromJson(json, ImgFlipMemesResponse.class);
        return memes.isSuccess() ? memes.getData().getMemes() : Collections.emptyList();
    }

    private static class ScoredMeme {

        final double score;
        final ImgFlipMeme meme;

        public ScoredMeme(double score, ImgFlipMeme meme) {
            this.score = score;
            this.meme = meme;
        }

        public double getScore() {
            return score;
        }

        public String getName() {
            return meme.getName();
        }

        public ImgFlipMeme getMeme() {
            return meme;
        }
    }

}
