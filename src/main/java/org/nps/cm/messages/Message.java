package org.nps.cm.messages;

import java.net.InetSocketAddress;
import java.time.Instant;
import java.util.UUID;

public class Message {

    final Instant time;
    final UUID target;
    final String author;
    final String message;

    InetSocketAddress address;

    public Message(Instant time, UUID target, String author, String message) {
        this.time = time;
        this.target = target;
        this.author = author;
        this.message = message;
    }

    public Instant getTime() {
        return time;
    }

    public boolean hasTarget() {
        return target != null;
    }

    public UUID getTarget() {
        return target;
    }

    public String getAuthor() {
        return author;
    }

    public String getMessage() {
        return message;
    }

}
