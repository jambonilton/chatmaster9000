package org.nps.cm.messages;

import com.google.gson.Gson;
import io.undertow.websockets.WebSocketConnectionCallback;
import io.undertow.websockets.core.*;
import io.undertow.websockets.spi.WebSocketHttpExchange;
import org.nps.cm.http.RestfulHttpHandler;
import org.nps.cm.log.Logger;
import org.nps.cm.log.Severity;
import org.nps.cm.sql.DAO;

import java.io.IOException;
import java.net.InetAddress;
import java.util.function.UnaryOperator;

public class MessageHttpHandler extends RestfulHttpHandler<Message, MessagesQuery> implements WebSocketConnectionCallback {

    final UnaryOperator<Message> messageFilter;

    public MessageHttpHandler(DAO<Message, MessagesQuery> dao, Gson gson, Logger logger, UnaryOperator<Message> messageFilter) {
        super(logger, dao, gson, Message.class, MessagesQuery.class);
        this.messageFilter = messageFilter;
    }

    @Override
    public void onConnect(WebSocketHttpExchange exchange, WebSocketChannel channel) {

        channel.getReceiveSetter().set(new AbstractReceiveListener() {

            @Override
            protected void onFullTextMessage(WebSocketChannel channel, BufferedTextMessage message) {
                final String messageData = message.getData();
                try {
                    final InetAddress address = channel.getSourceAddress().getAddress();
                    logger.log(address + " " + channel.getUrl() + " " + messageData);
                    // lookupUserFromAddress(address);

                    final Message chatMessage = messageFilter.apply(gson.fromJson(messageData, Message.class));

                    dao.insert(chatMessage);

                    final String persistedMessageJson = gson.toJson(chatMessage);
                    for (WebSocketChannel session : channel.getPeerConnections()) {
                        WebSockets.sendText(persistedMessageJson, session, null);
                    }
                } catch (Exception e) {
                    logger.error(Severity.MAJOR, e, "Failed to save message. It is lost now.");
                }
            }

            @Override
            protected void onClose(WebSocketChannel webSocketChannel, StreamSourceFrameChannel channel) throws IOException {
                super.onClose(webSocketChannel, channel);
            }

        });

        channel.resumeReceives();
    }

//    private privatevoid lookupUserFromAddress(InetAddress address) throws java.sql.SQLException, IllegalAccessException {
//        final List<ChatUser> users = usersDAO.list(new ChatUsersQuery().setAddress(address));
//        if (users.isEmpty()) {
//            usersDAO.insert(new ChatUser(address, AccessLevel.PLEB));
//        } else if (users.get(0).getAccessLevel().equals(AccessLevel.CON)) {
//            throw new IllegalAccessException("Banned user at "+users.get(0)+" attempting to send messages!");
//        }
//    }

}
