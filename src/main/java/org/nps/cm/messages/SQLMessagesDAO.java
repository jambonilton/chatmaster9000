package org.nps.cm.messages;

import org.nps.cm.sql.SQLDatabase;
import org.nps.cm.sql.exec.SQLExecutor;
import org.nps.cm.sql.gen.SQLQueryable;
import org.nps.cm.stats.AggregateQuery;
import org.nps.cm.stats.ChartDataMapper;
import org.nps.cm.util.Comparators;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import java.util.stream.Stream;

import static org.nps.cm.sql.gen.SQLDeleteBuilder.deleteFrom;
import static org.nps.cm.sql.gen.SQLInsertBuilder.insertInto;
import static org.nps.cm.sql.gen.SQLQueryBuilder.eq;
import static org.nps.cm.sql.gen.SQLQueryBuilder.select;

public class SQLMessagesDAO implements MessagesDAO {

    final SQLDatabase database;
    final SQLExecutor executor;

    public SQLMessagesDAO(SQLDatabase database) {
        this.database = database;
        this.executor = new SQLExecutor(database);
    }

    @Override
    public void insert(Message message) throws SQLException {
        executor.apply(insertInto("Messages", "target", "author", "message")
                .values(message.getTarget(), message.getAuthor(), message.getMessage()));
    }

    @Override
    public Stream<Message> list(MessagesQuery mq) throws SQLException {
        final SQLQueryable query = select().from("Messages")
                .where(eq("target", mq.getTarget()))
                .limit(mq.getLimit())
                .offset(mq.getOffset())
                .orderByDescending("time");
        return executor.query(query, this::toMessage)
                .sorted(Comparators.mapping(Message::getTime));
    }

    @Override
    public Message get(UUID id) throws SQLException {
        return executor.query(select().from("Messages").where(eq("id", id)), this::toMessage).findFirst().get();
    }

    private Message toMessage(ResultSet rs) throws SQLException {
        return new Message(rs.getTimestamp(1).toInstant(), getUUID(rs), rs.getString(3), rs.getString(4));
    }

    @Override
    public void remove(UUID id) throws SQLException {
        executor.apply(deleteFrom("Messages").where(eq("id", id)));
    }

    private UUID getUUID(ResultSet rs) throws SQLException {
        if (database.supportsUUID())
            return rs.getObject(2, UUID.class);
        else {
            final String id = rs.getString(2);
            if (id == null)
                return null;
            return UUID.fromString(id);
        }
    }

    @Override
    public Stream<Object[]> aggregate(AggregateQuery aggregateQuery, MessagesQuery query) throws SQLException {
        final SQLQueryable select = select(aggregateQuery.getKeySelect(), aggregateQuery.getFieldWithOperation())
                                    .from("ChannelMessages")
                                    .groupBy(aggregateQuery.getKey());
        return executor.query(select, new ChartDataMapper(aggregateQuery));
    }

}
