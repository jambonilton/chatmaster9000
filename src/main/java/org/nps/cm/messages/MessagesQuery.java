package org.nps.cm.messages;

import java.util.UUID;

public class MessagesQuery {

    UUID target;
    Integer limit;
    Integer offset;

    public MessagesQuery setTarget(UUID target) {
        this.target = target;
        return this;
    }

    public UUID getTarget() {
        return target;
    }

    public Integer getLimit() {
        return limit;
    }

    public MessagesQuery setLimit(Integer limit) {
        this.limit = limit;
        return this;
    }

    public Integer getOffset() {
        return offset;
    }

    public MessagesQuery setOffset(Integer offset) {
        this.offset = offset;
        return this;
    }
}
