package org.nps.cm.stats;

import org.nps.cm.sql.exec.SQLResultMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class ChartDataMapper implements SQLResultMapper<Object[]> {

    final AggregateQuery query;

    public ChartDataMapper(AggregateQuery query) {
        this.query = query;
    }

    @Override
    public Object[] map(ResultSet rs) throws SQLException {
        final Object value = rs.getObject(AggregateQuery.VALUE);
        if (value == null)
            return new Object[]{rs.getObject(query.getKey()), null};
        else if (value instanceof Number)
            return new Object[]{rs.getObject(query.getKey()), value};
        else if (value instanceof Date)
            return new Object[]{rs.getObject(query.getKey()), ((Date) value).getTime()};
        else
            throw new UnsupportedOperationException("Non-numeric type for aggregation "+value.getClass());
    }

}
