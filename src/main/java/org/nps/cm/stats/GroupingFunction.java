package org.nps.cm.stats;

public enum GroupingFunction {
    SUM, AVG, COUNT
}