package org.nps.cm.stats;

public class AggregateQuery {

    public static final String VALUE = "value";

    String target;
    String key;
    GroupingFunction operation;
    Long bucketSize;

    public String getFieldWithOperation() {
        return getOperation() + "(" + getTarget() + ") as "+VALUE;
    }

    public String getTarget() {
        return target == null || target.isEmpty() ? "*" : target;
    }

    public AggregateQuery setTarget(String target) {
        this.target = target;
        return this;
    }

    public String getKeySelect() {
        return isBucketed()
                ? "("+getBucketSize()+"*FLOOR("+key+"/"+getBucketSize()+")) as "+getKey()
                : getKey();
    }

    public String getKey() {
        return isBucketed()
            ? key + "_bucket" : key;
    }

    public AggregateQuery setKey(String key) {
        this.key = key;
        return this;
    }

    public GroupingFunction getOperation() {
        return operation;
    }

    public AggregateQuery setOperation(GroupingFunction operation) {
        this.operation = operation;
        return this;
    }

    public boolean isBucketed() {
        return bucketSize != null;
    }

    public Long getBucketSize() {
        return bucketSize;
    }

    public AggregateQuery setBucketSize(Long bucketSize) {
        this.bucketSize = bucketSize;
        return this;
    }

}
