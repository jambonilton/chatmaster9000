package org.nps.cm;

import io.undertow.server.HttpHandler;
import org.nps.cm.http.StaticResponseHandler;
import org.nps.cm.io.Resources;

import static java.util.stream.Collectors.joining;

public class Handlers {

    /**
     * Recursively concatenates all non-folder files in resource path.
     * @param path Resource path.
     * @param contentType Content-type header to serve back in response.
     * @return All files concatenated.
     */
    public static HttpHandler mergingResourcesHandler(String path, String contentType) {
        return new StaticResponseHandler(Resources.getFilesInPackage(path).map(Resources::toString).collect(joining()), contentType);
    }

}
