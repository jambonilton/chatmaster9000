package org.nps.cm.http;

import com.google.gson.Gson;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HeaderValues;
import io.undertow.util.Headers;
import org.nps.cm.log.Logger;
import org.nps.cm.sql.DAO;
import org.nps.cm.stats.AggregateQuery;
import org.nps.cm.stats.GroupingFunction;

import java.io.InputStreamReader;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toList;

public class RestfulHttpHandler<E, Q> implements HttpHandler {

    private static final long DEFAULT_LIMIT = 100;
    private static Pattern AGGREGATE_PATTERN = Pattern.compile("(\\p{Alpha}+)_(\\p{Alpha}*)_?by_(\\p{Alpha}+)");

    protected final Logger logger;
    protected final DAO<E, Q> dao;
    protected final Gson gson;
    protected final Class<E> elementType;
    protected final Class<Q> queryType;

    public RestfulHttpHandler(Logger logger, DAO<E, Q> dao, Gson gson, Class<E> elementType, Class<Q> queryType) {
        this.logger = logger;
        this.dao = dao;
        this.gson = gson;
        this.elementType = elementType;
        this.queryType = queryType;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        logger.log("API call "+exchange.getRequestMethod().toString()+" "+exchange.getRequestPath());
        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
        getResponse(exchange).ifPresent(exchange.getResponseSender()::send);
    }

    private Optional<String> getResponse(HttpServerExchange exchange) throws Exception {
        final String method = exchange.getRequestMethod().toString();
        switch (method) {
            case "GET":
                final Optional<UUID> uuid = tryUUID(exchange);
                if (uuid.isPresent())
                    return Optional.of(gson.toJson(dao.get(uuid.get())));
                final Optional<AggregateQuery> statsQuery = tryAggregate(exchange);
                final Q queryFromParams = UrlFormReflection.getQueryObject(exchange, queryType);
                if (statsQuery.isPresent()) {
                    final List<Object[]> result = dao.aggregate(statsQuery.get(), queryFromParams)
                            .limit(getLimit(exchange))
                            .collect(toList());
                    return Optional.of(gson.toJson(result));
                }
                final List<E> result = dao.list(queryFromParams)
                        .limit(getLimit(exchange))
                        .collect(toList());
                return Optional.of(gson.toJson(result));
            case "POST":
                dao.insert(gson.fromJson(new InputStreamReader(exchange.getInputStream()), elementType));
                return Optional.empty();
            case "DELETE":
                dao.remove(tryUUID(exchange).orElseThrow(() -> new IllegalArgumentException("Invalid UUID in path "+exchange.getRequestPath())));
            default: throw new UnsupportedOperationException("Unsupported method "+method);
        }
    }

    private Optional<UUID> tryUUID(HttpServerExchange exchange) {
        try {
            final String path = exchange.getRequestPath();
            final String lastSegment = path.substring(path.lastIndexOf('/') + 1);
            logger.log(lastSegment);
            return Optional.of(UUID.fromString(lastSegment));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    private Optional<AggregateQuery> tryAggregate(HttpServerExchange exchange) {
        final String path = exchange.getRequestPath();
        final String lastSegment = path.substring(path.lastIndexOf('/') + 1);
        final Matcher matcher = AGGREGATE_PATTERN.matcher(lastSegment);
        if (matcher.matches()) {
            final AggregateQuery query = new AggregateQuery()
                    .setOperation(GroupingFunction.valueOf(matcher.group(1).toUpperCase()))
                    .setTarget(matcher.group(2))
                    .setKey(matcher.group(3))
                    .setBucketSize(getBucketSize(exchange));
            return Optional.of(query);
        } else {
            return Optional.empty();
        }
    }

    private Long getBucketSize(HttpServerExchange exchange) {
        final HeaderValues value = exchange.getRequestHeaders().get("bucket-size");
        if (value == null)
            return null;
        return value.stream().findFirst().map(String::trim).map(Long::new).orElse(null);
    }

    private long getLimit(HttpServerExchange exchange) {
        final HeaderValues value = exchange.getRequestHeaders().get("limit");
        if (value == null)
            return DEFAULT_LIMIT;
        return value.stream().findFirst().map(String::trim).map(Long::new).orElse(DEFAULT_LIMIT);
    }

}
