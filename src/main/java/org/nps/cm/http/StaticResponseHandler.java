package org.nps.cm.http;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.HttpString;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class StaticResponseHandler implements HttpHandler {

    final ByteBuffer content;
    final String contentType;

    public StaticResponseHandler(String response, String contentType) {
        this(response.getBytes(Charset.defaultCharset()), contentType);
    }

    public StaticResponseHandler(byte[] response, String contentType) {
        this.content = ByteBuffer.wrap(response);
        this.contentType = contentType;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, contentType);
        exchange.getResponseHeaders().put(HttpString.tryFromString("Content-Security-Policy"), "frame-ancestors https://commandcenter.radian6.com");
        exchange.getResponseHeaders().put(HttpString.tryFromString("X-Frame-Options"), "SAMEORIGIN");
        exchange.getResponseSender().send(content.duplicate());
    }

}
