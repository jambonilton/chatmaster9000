package org.nps.cm.http;

import io.undertow.server.HttpServerExchange;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.Deque;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;
import static org.nps.cm.util.Converter.convertTo;

public class UrlFormReflection {

    public static <Q> Q getQueryObject(HttpServerExchange exchange, Class<Q> queryType) throws ReflectiveOperationException {
        final Q query = queryType.newInstance();
        for (Field field : queryType.getDeclaredFields())
            getParam(exchange, field.getName()).ifPresent(value -> setValue(query, field, value));
        return query;
    }

    private static Optional<String> getParam(HttpServerExchange exchange, String field) {
        final Deque<String> values = exchange.getQueryParameters().get(field);
        return values == null
                ? Optional.empty()
                : values.stream().findFirst();
    }

    private static <Q> void setValue(Q query, Field field, String value) {
        try {
            field.setAccessible(true);
            field.set(query, convertTo(value, field.getType()));
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getQueryString(Object object) {
        if (object instanceof Map) {
            final Map<Object, Object> map = (Map) object;
            return map.entrySet().stream()
                    .map(e -> toQuery(e.getKey(), e.getValue()))
                    .collect(joining("&"));
        } else {
            return Stream.of(object.getClass().getDeclaredFields())
                    .map(f -> toQuery(f, object))
                    .filter(e -> e != null)
                    .collect(joining("&"));
        }
    }

    private static String toQuery(Field field, Object obj) {
        try {
            field.setAccessible(true);
            final Object value = field.get(obj);
            if (value == null)
                return null;
            return toQuery(field.getName(), value);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    private static String toQuery(Object key, Object value) {
        try {
            return key + "=" + URLEncoder.encode(String.valueOf(value), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

}
