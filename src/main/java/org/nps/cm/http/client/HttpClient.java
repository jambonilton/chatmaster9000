package org.nps.cm.http.client;

public interface HttpClient {

    HttpResponse send(HttpRequest request);

}
