package org.nps.cm.users;

import java.net.InetAddress;

public class ChatUser {

    final InetAddress address;
    final AccessLevel accessLevel;

    public ChatUser(InetAddress address, AccessLevel accessLevel) {
        this.address = address;
        this.accessLevel = accessLevel;
    }

    public InetAddress getAddress() {
        return address;
    }

    public AccessLevel getAccessLevel() {
        return accessLevel;
    }

}
