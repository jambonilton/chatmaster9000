package org.nps.cm.users;

import java.net.InetAddress;

public class ChatUsersQuery {

    private InetAddress address;

    public InetAddress getAddress() {
        return address;
    }

    public ChatUsersQuery setAddress(InetAddress address) {
        this.address = address;
        return this;
    }

}
