package org.nps.cm.users;

import org.nps.cm.sql.DAO;

public interface ChatUsersDAO extends DAO<ChatUser, ChatUsersQuery> {

}
