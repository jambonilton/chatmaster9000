package org.nps.cm.users;

import org.nps.cm.sql.SQLDatabase;
import org.nps.cm.sql.exec.SQLExecutor;
import org.nps.cm.sql.gen.SQLQueryable;
import org.nps.cm.stats.AggregateQuery;
import org.nps.cm.stats.ChartDataMapper;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import java.util.stream.Stream;

import static org.nps.cm.sql.gen.SQLDeleteBuilder.deleteFrom;
import static org.nps.cm.sql.gen.SQLInsertBuilder.insertInto;
import static org.nps.cm.sql.gen.SQLQueryBuilder.eq;
import static org.nps.cm.sql.gen.SQLQueryBuilder.select;

public class SQLChatUsersDAO implements ChatUsersDAO {

    final SQLDatabase database;
    final SQLExecutor executor;

    public SQLChatUsersDAO(SQLDatabase database) {
        this.database = database;
        this.executor = new SQLExecutor(database);
    }

    @Override
    public void insert(ChatUser user) throws SQLException {
        executor.apply(insertInto("ChatUsers", "address", "accessLevel")
                .values(user.getAddress().getAddress(), user.getAccessLevel().ordinal()));
    }

    @Override
    public Stream<ChatUser> list(ChatUsersQuery query) throws SQLException {
        return executor.query(select().from("ChatUsers"), this::toChatUser);
    }

    @Override
    public ChatUser get(UUID id) throws SQLException {
        return executor.query(select().from("ChatUsers").where(eq("id", id)), this::toChatUser).findFirst().get();
    }

    private ChatUser toChatUser(ResultSet rs) throws SQLException {
        return new ChatUser(getAddress(rs), AccessLevel.values()[rs.getInt(2)]);
    }

    @Override
    public void remove(UUID id) throws SQLException {
        executor.apply(deleteFrom("ChatUsers").where(eq("id", id)));
    }

    private InetAddress getAddress(ResultSet rs) throws SQLException {
        try {
            if (database.supportsInetAddress())
                return rs.getObject(1, InetAddress.class);
            final byte[] bytes = rs.getBytes(1);
            return InetAddress.getByAddress(bytes);
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Stream<Object[]> aggregate(AggregateQuery aggregateQuery, ChatUsersQuery query) throws SQLException {
        final SQLQueryable select = select(aggregateQuery.getKeySelect(), aggregateQuery.getFieldWithOperation())
                .from("ChatUsers")
                .groupBy(aggregateQuery.getKey());
        return executor.query(select, new ChartDataMapper(aggregateQuery));
    }
}
