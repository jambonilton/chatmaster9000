package org.nps.cm.users;

public enum AccessLevel {
    PLEB,  // general population
    CON,   // banned
    MASTER // total access
}
