package org.nps.cm.sql.exec;

import org.nps.cm.sql.SQLDatabase;
import org.nps.cm.sql.gen.SQLExecutable;
import org.nps.cm.sql.gen.SQLQueryable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Stream;

import static org.nps.cm.util.Streams.autoClosing;
import static org.nps.cm.util.Streams.toStream;

public class SQLExecutor {

    final SQLDatabase database;

    public SQLExecutor(SQLDatabase database) {
        this.database = database;
    }

    public <E> Stream<E> query(SQLQueryable queryable, SQLResultMapper<E> mapper) throws SQLException {
        final Connection conn = database.getConnection();
        final PreparedStatement preparedStatement = conn.prepareStatement(queryable.toPreparedString());
        for (int i=0; i < queryable.getArguments().length; i++) {
            Object value = queryable.getArguments()[i];
            if (value instanceof UUID && !database.supportsUUID())
                value = value.toString();

            preparedStatement.setObject(i+1, value);
        }
        final ResultSet resultSet = preparedStatement.executeQuery();
        final Stream<E> results = toStream(new ResultSetIterator(resultSet))
                .onClose(() -> close(conn, preparedStatement, resultSet))
                .map(wrap(mapper));
        return autoClosing(results);
    }

    public int apply(SQLExecutable update) throws SQLException {
        try (final Connection conn = database.getConnection()) {
            try (final PreparedStatement ps = conn.prepareStatement(update.toPreparedString())) {
                for (int i=0; i < update.getArguments().length; i++) {
                    Object value = update.getArguments()[i];
                    if (value instanceof UUID && !database.supportsUUID())
                        value = value.toString();
                    ps.setObject(i+1, value);
                }
                return ps.executeUpdate();
            }
        }
    }

    private void close(Connection conn, PreparedStatement ps, ResultSet rs) {
        try {
            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
    }

    private <E> Function<ResultSet, E> wrap(SQLResultMapper<E> mapper) {
        return resultSet -> {
            try {
                return mapper.map(resultSet);
            } catch (SQLException e) {
                throw new SQLRuntimeException(e);
            }
        };
    }

}
