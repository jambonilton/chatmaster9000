package org.nps.cm.sql.postgres;

import org.nps.cm.sql.SQLDatabase;
import org.postgresql.Driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class HerokuPostgresDatabase implements SQLDatabase {

    @Override
    public Connection getConnection() throws SQLException {
        try {
            Class.forName(Driver.class.getName()); // get driver into ClassLoader
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Could not find driver for database, cannot continue.", e);
        }
        String dbUrl = System.getenv("JDBC_DATABASE_URL");
        return DriverManager.getConnection(dbUrl);
    }

    @Override
    public boolean supportsUUID() {
        return true;
    }

    @Override
    public boolean supportsInetAddress() {
        return true;
    }
}
