package org.nps.cm.sql.gen;

public class SQLDeleteBuilder {

    public static SQLUpdateBuilder deleteFrom(String table) {
        return new SQLUpdateBuilder(table);
    }

    private final String table;

    SQLDeleteBuilder(String table) {
        this.table = table;
    }

    public SQLExecutable where(SQLQueryBuilder.WhereClause where) {
        return new SQLExecutable() {
            @Override
            public String toPreparedString() {
                return "DELETE FROM "+table+ " WHERE " + where.toPreparedSQL();
            }
            @Override
            public Object[] getArguments() {
                return where.getArguments();
            }
            @Override
            public String toString() {
                return this.toPreparedString();
            }
        };
    }

}
