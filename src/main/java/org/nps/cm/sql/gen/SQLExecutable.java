package org.nps.cm.sql.gen;

public interface SQLExecutable {

    String toPreparedString();

    Object[] getArguments();

}
