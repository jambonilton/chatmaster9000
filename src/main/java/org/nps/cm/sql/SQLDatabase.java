package org.nps.cm.sql;

import java.sql.Connection;
import java.sql.SQLException;

public interface SQLDatabase {

    Connection getConnection() throws SQLException;

    default boolean shouldRetry(SQLException e) {
        return false;
    }

    default boolean supportsUUID() {
        return false;
    }

    default boolean supportsInetAddress() {
        return false;
    }

}
