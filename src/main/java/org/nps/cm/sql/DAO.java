package org.nps.cm.sql;

import org.nps.cm.stats.AggregateQuery;

import java.sql.SQLException;
import java.util.UUID;
import java.util.stream.Stream;

public interface DAO<E,Q> {

    void insert(E item) throws SQLException;

    Stream<E> list(Q query) throws SQLException;

    void remove(UUID id) throws SQLException;

    E get(UUID id) throws SQLException;

    Stream<Object[]> aggregate(AggregateQuery aggregateQuery, Q query) throws SQLException;

}
