package org.nps.cm.sql;

import org.nps.cm.stats.AggregateQuery;

import java.sql.SQLException;
import java.util.UUID;
import java.util.stream.Stream;

public class RetryingDAO<E,Q> implements DAO<E,Q> {

    final DAO<E,Q> base;
    final SQLDatabase database;

    public RetryingDAO(DAO<E, Q> base, SQLDatabase database) {
        this.base = base;
        this.database = database;
    }

    @Override
    public void insert(E item) throws SQLException {
        try {
            base.insert(item);
        } catch (SQLException e) {
            if (database.shouldRetry(e))
                insert(item);
            throw e;
        }
    }

    @Override
    public Stream<E> list(Q query) throws SQLException {
        try {
            return base.list(query);
        } catch (SQLException e) {
            if (database.shouldRetry(e))
                return list(query);
            throw e;
        }
    }

    @Override
    public E get(UUID id) throws SQLException {
        try {
            return base.get(id);
        } catch (SQLException e) {
            if (database.shouldRetry(e))
                return get(id);
            throw e;
        }
    }

    @Override
    public void remove(UUID id) throws SQLException {
        try {
            base.remove(id);
        } catch (SQLException e) {
            if (database.shouldRetry(e))
                remove(id);
            throw e;
        }
    }

    @Override
    public Stream<Object[]> aggregate(AggregateQuery aggregateQuery, Q query) throws SQLException {
        try {
            return base.aggregate(aggregateQuery, query);
        } catch (SQLException e) {
            if (database.shouldRetry(e))
                return aggregate(aggregateQuery, query);
            throw e;
        }
    }
}
