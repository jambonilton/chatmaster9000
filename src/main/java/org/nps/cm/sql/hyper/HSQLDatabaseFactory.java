package org.nps.cm.sql.hyper;

import org.nps.cm.sql.SQLDatabase;

import java.io.File;

public final class HSQLDatabaseFactory {

    public static SQLDatabase forFile(String file) {
        final AutoInitHSQLDatabase db = new AutoInitHSQLDatabase(new HSQLFileDatabase(file),
                "/hsql/channels.sql", "/hsql/messages.sql", "/hsql/chatusers.sql", "/hsql/messages_channels_view.sql");
        if (!new File(file).exists())
            db.initTables();
        return db;
    }

}
