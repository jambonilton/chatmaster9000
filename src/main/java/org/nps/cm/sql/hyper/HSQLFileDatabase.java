package org.nps.cm.sql.hyper;

import org.nps.cm.sql.SQLDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

class HSQLFileDatabase implements SQLDatabase {

    final String file;

    public HSQLFileDatabase(String file) {
        this.file = file;
    }

    @Override
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:hsqldb:file:"+file+"/data", "SA", "");
    }

}
