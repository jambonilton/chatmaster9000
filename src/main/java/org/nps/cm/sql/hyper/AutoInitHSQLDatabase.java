package org.nps.cm.sql.hyper;

import org.nps.cm.io.Resources;
import org.nps.cm.sql.SQLDatabase;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

class AutoInitHSQLDatabase implements SQLDatabase {

    private final SQLDatabase base;
    private final String[] sqlScripts;

    public AutoInitHSQLDatabase(SQLDatabase base, String... sqlScripts) {
        this.base = base;
        this.sqlScripts = sqlScripts;
    }

    @Override
    public Connection getConnection() throws SQLException {
        return base.getConnection();
    }

    @Override
    public boolean shouldRetry(SQLException e) {
        if (e.getMessage() != null && e.getMessage().contains("object not found"))
            return initTables();
        return base.shouldRetry(e);
    }

    boolean initTables() {
        try (final Connection conn = base.getConnection()) {
            try (final Statement st = conn.createStatement()) {
                for (String script : sqlScripts) {
                    final String sql = Resources.toString(script);
                    st.execute(sql);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
