package org.nps.cm;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.undertow.Undertow;
import io.undertow.UndertowOptions;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.resource.ClassPathResourceManager;
import io.undertow.server.handlers.resource.ResourceHandler;
import io.undertow.websockets.WebSocketProtocolHandshakeHandler;
import org.nps.cm.channels.CreateChannelHttpHandler;
import org.nps.cm.channels.GetChannelsHttpHandler;
import org.nps.cm.channels.SQLChannelsDAO;
import org.nps.cm.http.StaticResponseHandler;
import org.nps.cm.http.client.BasicHttpClient;
import org.nps.cm.io.Resources;
import org.nps.cm.json.InstantAdapter;
import org.nps.cm.log.Logger;
import org.nps.cm.log.LoggingHttpHandler;
import org.nps.cm.messages.Message;
import org.nps.cm.messages.MessageHttpHandler;
import org.nps.cm.messages.SQLMessagesDAO;
import org.nps.cm.messages.filters.gifs.GiphyGifTranslator;
import org.nps.cm.messages.filters.memes.ImgflipMemesFilter;
import org.nps.cm.sql.SQLDatabase;
import org.nps.cm.sql.hyper.HSQLDatabaseFactory;
import org.nps.cm.sql.postgres.HerokuPostgresDatabase;
import org.nps.cm.util.Functions;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.time.Instant;
import java.util.function.UnaryOperator;

import static io.undertow.Handlers.path;
import static io.undertow.Handlers.websocket;

public class ChatMaster {

    static final ChatMasterProperties properties = new ChatMasterProperties();

    public static void main(String[] args) throws Exception {

        try {

            final Logger logger = Logger.console();
            final String mode = args.length > 0 ? args[0] : "prod";

            final HttpHandler indexHandler = new StaticResponseHandler(Resources.toString("web/index.html"), "text/html");
            final SQLDatabase database = mode.equals("prod") ? new HerokuPostgresDatabase() : HSQLDatabaseFactory.forFile(properties.getDataFile());
            final Gson gson = new GsonBuilder().registerTypeAdapter(Instant.class, new InstantAdapter()).create();
            final BasicHttpClient client = new BasicHttpClient();
            final UnaryOperator<Message> messageFilter = Functions.chain(new GiphyGifTranslator(client, gson), new ImgflipMemesFilter(logger, client, gson));
            final MessageHttpHandler messageHandler = new MessageHttpHandler(new SQLMessagesDAO(database), gson, logger, messageFilter);
            final GetChannelsHttpHandler channelsHandler = new GetChannelsHttpHandler(logger, new SQLChannelsDAO(database), gson);
            final HttpHandler pingService = exchange -> exchange.getResponseSender().send("pong");

            final WebSocketProtocolHandshakeHandler chatHandler = websocket(messageHandler);

            HttpHandler handler = path()
                    .addPrefixPath("/chat", chatHandler)
                    .addPrefixPath("/messages", messageHandler)
                    .addPrefixPath("/channels", channelsHandler)
                    .addPrefixPath("/ping", pingService)
                    .addExactPath("/newChannel", new CreateChannelHttpHandler(new SQLChannelsDAO(database)))
                    .addExactPath("/", indexHandler)
                    .addExactPath("/help", new StaticResponseHandler(Resources.toString("web/help.html"), "text/html"))
                    .addExactPath("/usage", new StaticResponseHandler(Resources.toString("web/usage.html"), "text/html"))
                    .addPrefixPath("/c/", indexHandler)
                    .addExactPath("/script.js", Handlers.mergingResourcesHandler("web/js", "application/javascript"))
                    .addExactPath("/style.css", Handlers.mergingResourcesHandler("web/css", "text/css"))
                    .addPrefixPath("/static", new ResourceHandler(new ClassPathResourceManager(ChatMaster.class.getClassLoader(), "web/static")));
            handler = new LoggingHttpHandler(logger, handler);

            final InetAddress localhost = Inet4Address.getLocalHost();
            logger.log("Starting server on " + localhost + ", port " + properties.getPort());

            final Undertow server = Undertow.builder()
                    .setServerOption(UndertowOptions.ENABLE_HTTP2, true)
                    .setServerOption(UndertowOptions.ENABLE_SPDY, true)
                    .addHttpListener(properties.getPort(), "0.0.0.0")
                    .setHandler(handler)
                    .build();

            server.start();

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

}
