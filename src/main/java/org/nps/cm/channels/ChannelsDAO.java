package org.nps.cm.channels;

import org.nps.cm.sql.DAO;

import java.sql.SQLException;
import java.util.UUID;

public interface ChannelsDAO extends DAO<Channel, ChannelsQuery> {

    Channel get(UUID id) throws SQLException;

}
