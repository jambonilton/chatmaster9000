package org.nps.cm.channels;

import com.google.gson.Gson;
import org.nps.cm.http.RestfulHttpHandler;
import org.nps.cm.log.Logger;
import org.nps.cm.sql.DAO;

public class GetChannelsHttpHandler extends RestfulHttpHandler<Channel, ChannelsQuery> {

    public GetChannelsHttpHandler(Logger logger, DAO<Channel, ChannelsQuery> dao, Gson gson) {
        super(logger, dao, gson, Channel.class, ChannelsQuery.class);
    }

}