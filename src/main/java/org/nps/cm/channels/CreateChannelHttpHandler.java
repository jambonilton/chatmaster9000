package org.nps.cm.channels;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.form.FormData;
import io.undertow.server.handlers.form.FormDataParser;
import io.undertow.server.handlers.form.FormParserFactory;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;

public class CreateChannelHttpHandler implements HttpHandler {

    final ChannelsDAO dao;

    public CreateChannelHttpHandler(ChannelsDAO dao) {
        this.dao = dao;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        final FormDataParser formDataParser = FormParserFactory.builder().build().createParser(exchange);
        if (formDataParser == null)
            throw new RuntimeException("Failed to get form parser");
        final FormData formData = formDataParser.parseBlocking();
        if (formData == null)
            throw new RuntimeException("Failed to get form data");

        final String name = formData.getFirst("name").getValue();
        final String privacy = formData.getFirst("privacy").getValue();
        final Channel channel = new Channel(name, Privacy.valueOf(privacy.toUpperCase()));
        dao.insert(channel);

        exchange.setStatusCode(StatusCodes.FOUND);
        exchange.getResponseHeaders().put(Headers.LOCATION, "/c/"+channel.getId());
    }

}
