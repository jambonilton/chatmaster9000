package org.nps.cm.channels;

public enum Privacy {
    PUBLIC, HIDDEN
}