package org.nps.cm.channels;

import org.nps.cm.sql.SQLDatabase;
import org.nps.cm.sql.exec.SQLExecutor;
import org.nps.cm.sql.gen.SQLQueryable;
import org.nps.cm.stats.AggregateQuery;
import org.nps.cm.stats.ChartDataMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import java.util.stream.Stream;

import static org.nps.cm.sql.gen.SQLDeleteBuilder.deleteFrom;
import static org.nps.cm.sql.gen.SQLInsertBuilder.insertInto;
import static org.nps.cm.sql.gen.SQLQueryBuilder.eq;
import static org.nps.cm.sql.gen.SQLQueryBuilder.select;

public class SQLChannelsDAO implements ChannelsDAO {

    final SQLDatabase database;
    final SQLExecutor executor;

    public SQLChannelsDAO(SQLDatabase database) {
        this.database = database;
        this.executor = new SQLExecutor(database);
    }

    public void insert(Channel channel) throws SQLException {
        executor.apply(insertInto("Channels", "id", "name", "privacy")
                .values(channel.getId(), channel.getName(), channel.getPrivacy().ordinal()));
    }

    @Override
    public Channel get(UUID id) throws SQLException {
        return executor.query(select("id", "name", "privacy", "MAX(time) as time").from("Channels")
                .leftJoin("Messages", "id", "target")
                .where(eq("id", id))
                .groupBy("id", "name", "privacy")
                .orderByDescending("time"), this::toChannel).findAny().get();
    }

    @Override
    public Stream<Channel> list(ChannelsQuery query) throws SQLException {
        return executor.query(select("id", "name", "privacy", "MAX(time) as time").from("Channels")
                .leftJoin("Messages", "id", "target")
                .where(eq("privacy", 0))
                .groupBy("id", "name", "privacy")
                .orderByDescending("time"), this::toChannel);
    }

    @Override
    public void remove(UUID id) throws SQLException {
        executor.apply(deleteFrom("Channels").where(eq("id", id)));
    }

    private Channel toChannel(ResultSet rs) throws SQLException {
        return new Channel(getUUID(rs), rs.getString(2), Privacy.values()[rs.getInt(3)], getLastUpdated(rs));
    }

    private UUID getUUID(ResultSet rs) throws SQLException {
        if (database.supportsUUID())
            return rs.getObject(1, UUID.class);
        return UUID.fromString(rs.getString(1));
    }

    private Instant getLastUpdated(ResultSet rs) throws SQLException {
        final Timestamp timestamp = rs.getTimestamp(4);
        return timestamp == null ? Instant.now() : timestamp.toInstant();
    }

    @Override
    public Stream<Object[]> aggregate(AggregateQuery aggregateQuery, ChannelsQuery query) throws SQLException {
        final SQLQueryable select = select(aggregateQuery.getKeySelect(), aggregateQuery.getFieldWithOperation())
                .from("Channels")
                .groupBy(aggregateQuery.getKey());
        return executor.query(select, new ChartDataMapper(aggregateQuery));
    }

}
