package org.nps.cm.channels;

import java.time.Instant;
import java.util.UUID;

public class Channel {

    final UUID id;
    final String name;
    final Privacy privacy;
    final Instant lastUpdated;

    public Channel(String name, Privacy privacy) {
        this(UUID.randomUUID(), name, privacy, Instant.now());
    }

    public Channel(UUID id, String name, Privacy privacy, Instant lastUpdated) {
        this.id = id;
        this.name = name;
        this.privacy = privacy;
        this.lastUpdated = lastUpdated;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Privacy getPrivacy() {
        return privacy;
    }

    public Instant getLastUpdated() {
        return lastUpdated;
    }
}
