package org.nps.cm;

public class ChatMasterProperties {

    private int port = initPort();

    private int initPort() {
        final String envPort = System.getenv("PORT");
        if (envPort != null)
            return Integer.parseInt(envPort);
        return 8080;
    }

    private String dataFile = "data";

    public int getPort() {
        return port;
    }

    public String getDataFile() {
        return dataFile;
    }
}
