package org.nps.cm.log;

public enum Severity {
    MINOR,
    MAJOR,
    SEVERE,
    FATAL
}
