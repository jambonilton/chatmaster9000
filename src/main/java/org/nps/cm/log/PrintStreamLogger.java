package org.nps.cm.log;

import java.io.PrintStream;
import java.time.Clock;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PrintStreamLogger implements Logger {

    final Clock clock;
    final PrintStream out, err;

    public PrintStreamLogger(Clock clock, PrintStream out, PrintStream err) {
        this.clock = clock;
        this.out = out;
        this.err = err;
    }

    @Override
    public void log(String message) {
        out.println(clock.instant() + " " + message);
    }

    @Override
    public void error(Severity severity, Throwable throwable, String message) {
        err.println(clock.instant() + " " + severity+" "+message+"\n\t"+throwable.getClass().getSimpleName()+": "+throwable.getMessage());
        err.println(Stream.of(throwable.getStackTrace()).map(traceElement -> "\n\t\t" + traceElement.getClassName()
                + '.' + traceElement.getMethodName() + '(' + traceElement.getLineNumber() + ')')
                .collect(Collectors.joining()));
    }

}
