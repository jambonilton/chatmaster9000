package org.nps.cm.util;

import java.util.Optional;

@FunctionalInterface
public interface Lookup<K,V> {
    Optional<V> find(K key);
}
