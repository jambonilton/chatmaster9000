package org.nps.cm.channels;

import org.junit.Before;
import org.junit.Test;
import org.nps.cm.sql.DatabaseTest;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SQLChannelsDAOTest extends DatabaseTest {

    private SQLChannelsDAO dao;

    @Before
    public void setUp() {
        dao = new SQLChannelsDAO(db);
    }

    @Test
    public void storeAndGet() throws Exception {
        dao.insert(new Channel("Funkytown", Privacy.PUBLIC));
        final List<Channel> channelList = dao.list(new ChannelsQuery()).collect(Collectors.toList());
        assertEquals(1, channelList.size());
        final Channel channel = channelList.get(0);
        assertEquals("Funkytown", channel.getName());
        assertEquals(Privacy.PUBLIC, channel.getPrivacy());
        assertNotNull(channel.getId());
        assertNotNull(channel.getLastUpdated());
    }

}
