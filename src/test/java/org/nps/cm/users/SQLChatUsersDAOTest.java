package org.nps.cm.users;

import org.junit.Before;
import org.junit.Test;
import org.nps.cm.sql.DatabaseTest;

import java.net.InetAddress;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class SQLChatUsersDAOTest extends DatabaseTest {

    ChatUsersDAO users;

    @Before
    public void setUp() throws Exception {
        this.users = new SQLChatUsersDAO(db);
    }

    @Test
    public void storeAndGet() throws Exception {
        final ChatUser user = new ChatUser(InetAddress.getLocalHost(), AccessLevel.MASTER);
        users.insert(user);
        final List<ChatUser> userList = users.list(new ChatUsersQuery().setAddress(InetAddress.getLocalHost())).collect(Collectors.toList());
        final ChatUser persisted = userList.iterator().next();
        assertEquals(user.getAddress(), persisted.getAddress());
        assertEquals(user.getAccessLevel(), persisted.getAccessLevel());
    }

}