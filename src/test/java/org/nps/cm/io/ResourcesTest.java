package org.nps.cm.io;

import org.junit.Test;

import static java.util.stream.Collectors.joining;
import static org.junit.Assert.assertEquals;

public class ResourcesTest {

    @Test
    public void testRecursiveResourceTraversal() {
        assertEquals("a.txt, b.json, c.html",
                Resources.getFilesInPackage("resourcetest").map(p -> p.getFileName()).map(Object::toString).collect(joining(", ")));
    }

    @Test
    public void testToString() {
        assertEquals(Resources.toString("resourcetest/a.txt"), "foo");
    }

}