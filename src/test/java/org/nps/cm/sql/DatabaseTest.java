package org.nps.cm.sql;

import org.junit.After;
import org.junit.Before;
import org.nps.cm.io.FileUtil;
import org.nps.cm.sql.hyper.HSQLDatabaseFactory;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class DatabaseTest {

    protected File file;
    protected SQLDatabase db;

    @Before
    public void makeDatabase() {
        file = new File(UUID.randomUUID().toString());
        db = HSQLDatabaseFactory.forFile(file.getAbsolutePath());
    }

    @After
    public void destroyDatabase() throws IOException {
        FileUtil.deleteRecursively(file);
    }

}
