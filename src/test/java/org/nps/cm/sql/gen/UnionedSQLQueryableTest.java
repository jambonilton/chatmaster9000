package org.nps.cm.sql.gen;

import org.junit.Test;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.nps.cm.sql.gen.SQLQueryBuilder.*;


public class UnionedSQLQueryableTest
{

    @Test(expected = IllegalArgumentException.class)
    public void failsWhenLessThanTwoSubQueriesSupplied()
    {
        new UnionedSQLQueryable(asList(select().from("TopicFilter")));
    }

    @Test(expected = IllegalArgumentException.class)
    public void failsWhenEmptyCollectionSuppliedToUtilityMethod()
    {
        union();
    }

    @Test
    public void preparedStatementAndArgsAreCorrect()
    {
        final SQLQueryable sql = union(
                select("postId").from("Post").where(eq("a", 1), lessThan("b", 2)),
                select("postId").from("Post").where(greaterThan("c", 3), like("d", "blah"))
        );
        assertEquals("(SELECT postId FROM Post WHERE a = ? AND b < ?) " +
                "UNION (SELECT postId FROM Post WHERE c > ? AND d LIKE ?)", sql.toPreparedString());
        assertArrayEquals(new Object[]{1, 2, 3, "blah"}, sql.getArguments());
    }

    @Test
    public void pagingForQuery()
    {
        final SQLQueryable sql = union(
                select().from("Table1"),
                select().from("Table2")
        ).orderBy("field").limit(100).offset(200);
        assertEquals("(SELECT * FROM Table1) UNION (SELECT * FROM Table2) " +
                "ORDER BY field LIMIT 100 OFFSET 200", sql.toPreparedString());
        assertArrayEquals(new Object[0], sql.getArguments());
    }

}