package org.nps.cm.sql.gen;

import org.junit.Test;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.nps.cm.sql.gen.SQLQueryBuilder.*;

public class SQLQueryBuilderTest
{

    @Test
    public void basic()
    {
        assertEquals("SELECT * FROM Post",
                select().from("Post").toString());
    }

    @Test
    public void withFields()
    {
        assertEquals("SELECT postId, title, content FROM Post",
                select("postId", "title", "content").from("Post").toString());
    }

    @Test
    public void withJoins()
    {
        assertEquals("SELECT * FROM TopicFilter JOIN TopicFilterAuthorization USING (topicFilterId) INNER JOIN " +
                        "ProjectMembership ON (TopicFilter.projectId = ProjectMembership.projectId)",
                select().from("TopicFilter").join("TopicFilterAuthorization", "topicFilterId")
                        .innerJoin("ProjectMembership", "projectId", "projectId").toString());
    }

    @Test
    public void withWhere()
    {
        SQLQueryBuilder query = select().from("TopicFilter").where(eq("topicFilterId", 123));
        assertEquals("SELECT * FROM TopicFilter WHERE topicFilterId = ?", query.toString());
        assertArrayEquals(new Object[]{123}, query.getArguments());
    }

    @Test
    public void complicatedWhere()
    {
        String expected = "SELECT tfa.topicFilterId, fg.* FROM FilterGroup fg " +
                "JOIN TopicFilterFilterGroupAssignment tfa ON (fg.filterGroupId = tfa.filterGroupId) " +
                "WHERE fg.active = ? AND tfa.topicFilterId IN (?) AND (fg.clientId = ? OR fg.clientId IS NULL)";
        SQLQueryBuilder actual = select("tfa.topicFilterId", "fg.*")
                .from("FilterGroup fg")
                .join("TopicFilterFilterGroupAssignment tfa", "filterGroupId", "filterGroupId")
                .where(and(eq("fg.active", 1), in("tfa.topicFilterId", asList(1, 2, 3)), or(eq("fg.clientId", 1), isNull("fg.clientId"))));
        assertEquals(expected, actual.toPreparedString());
        assertArrayEquals(new Object[]{1, asList(1, 2, 3), 1}, actual.getArguments());
    }

    @Test
    public void likeClause()
    {
        SQLQueryBuilder actual = select().from("TopicFilter").where(like("title", "%blah%"));
        assertEquals("SELECT * FROM TopicFilter WHERE title LIKE ?", actual.toPreparedString());
        assertArrayEquals(new Object[]{"%blah%"}, actual.getArguments());
    }

    @Test
    public void rangeInclusive()
    {
        String expected = "SELECT postId FROM Post WHERE publishedDate >= ? AND publishedDate <= ?";
        String actual = select("postId").from("Post").where(greaterThanEq("publishedDate", "2016-01-01T12:00:00.000Z"),
                lessThanEq("publishedDate", "2016-01-01T13:00:00.000Z")).toString();
        assertEquals(expected, actual);
    }

    @Test
    public void rangeExclusive()
    {
        String expected = "SELECT postId FROM Post WHERE publishedDate > ? AND publishedDate < ?";
        String actual = select("postId").from("Post").where(greaterThan("publishedDate", "2016-01-01T12:00:00.000Z"),
                lessThan("publishedDate", "2016-01-01T13:00:00.000Z")).toString();
        assertEquals(expected, actual);
    }

    @Test
    public void paging()
    {
        assertEquals("SELECT * FROM Post ORDER BY publishedDate",
                select().from("Post").orderBy("publishedDate").toString());
        assertEquals("SELECT * FROM Post LIMIT 100",
                select().from("Post").limit(100).toString());
        assertEquals("SELECT * FROM Post OFFSET 10",
                select().from("Post").offset(10).toString());
        assertEquals("SELECT * FROM Post ORDER BY publishedDate DESC",
                select().from("Post").orderByDescending("publishedDate").toString());
        assertEquals("SELECT * FROM Post ORDER BY publishedDate",
                select().from("Post").orderBy("publishedDate").toString());
    }

}