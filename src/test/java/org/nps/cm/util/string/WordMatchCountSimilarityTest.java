package org.nps.cm.util.string;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WordMatchCountSimilarityTest {

    @Test
    public void wordMatchSimilarity() {
        assertSimilarity(1.0, "test", "test");
        assertSimilarity(0.75, "test", "test more");
        assertSimilarity(0.75, "test", "more test", 1);
        assertSimilarity(0.875, "all the things", "test all the things", 1);
    }

    private void assertSimilarity(double expected, String s1, String s2) {
        assertSimilarity(expected, s1, s2, 0);
    }

    private void assertSimilarity(double expected, String s1, String s2, int cardinalityLenience) {
        final StringSimilarity similarity = new WordMatchCountSimilarity(cardinalityLenience, new BinaryStringSimilarity());
        final double actual = similarity.test(s1, s2);
        assertEquals(expected, actual, 0.001d);
    }

}