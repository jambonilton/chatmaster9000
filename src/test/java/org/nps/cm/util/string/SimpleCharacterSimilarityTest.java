package org.nps.cm.util.string;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SimpleCharacterSimilarityTest {

    @Test
    public void test() {
        assertSimilarity(1.0, "test", "test");
        assertSimilarity(0.9, "test", "testy");
        assertSimilarity(0.9, "test", "testy", 1);
        assertSimilarity(0.9, "test", "stest", 1);
        assertSimilarity(0.0, "test", "stest");
        assertSimilarity(0.8, "testy", "tasty", 1);
    }

    private void assertSimilarity(double expected, String s1, String s2) {
        assertSimilarity(expected, s1, s2, 0);
    }

    private void assertSimilarity(double expected, String s1, String s2, int cardinalityLenience) {
        final StringSimilarity similarity = new SimpleCharacterSimilarity(cardinalityLenience);
        final double actual = similarity.test(s1, s2);
        assertEquals(expected, actual, 0.001d);
    }

}