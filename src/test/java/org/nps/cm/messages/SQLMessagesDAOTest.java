package org.nps.cm.messages;

import org.junit.Before;
import org.junit.Test;
import org.nps.cm.sql.DatabaseTest;
import org.nps.cm.stats.AggregateQuery;
import org.nps.cm.stats.GroupingFunction;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SQLMessagesDAOTest extends DatabaseTest {

    static final String MESSAGE = "Test";

    static SQLMessagesDAO dao;

    @Before
    public void setUp() throws Exception {
        dao = new SQLMessagesDAO(db);
    }

    @Test
    public void storeAndGet() throws Exception {
        final UUID target = UUID.randomUUID();
        dao.insert(new Message(null, target, "author", "message"));
        final List<Message> messages = dao.list(new MessagesQuery().setTarget(target)).collect(Collectors.toList());
        assertEquals(1, messages.size());
        assertEquals(target, messages.get(0).getTarget());
        assertEquals("author", messages.get(0).getAuthor());
        assertEquals("message", messages.get(0).getMessage());
        assertNotNull(messages.get(0).getTime());
    }

    @Test
    public void aggreate() throws Exception {
        final String authorOne = "steve", authorTwo = "bob";
        dao.insert(new Message(null, null, authorOne, MESSAGE));
        dao.insert(new Message(null, null, authorOne, MESSAGE));
        dao.insert(new Message(null, null, authorTwo, MESSAGE));
        dao.insert(new Message(null, null, authorTwo, MESSAGE));
        dao.insert(new Message(null, null, authorTwo, MESSAGE));
        final List<Object[]> counts = dao.aggregate(new AggregateQuery().setKey("author").setOperation(GroupingFunction.COUNT), new MessagesQuery())
                .collect(Collectors.toList());
        assertEquals(2L, counts.get(0)[1]);
        assertEquals(3L, counts.get(1)[1]);
    }

}