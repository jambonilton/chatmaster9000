package org.nps.cm.messages.filters.gifs;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class GiphyGifTranslatorTest {

    @Test
    public void testRegex() {
        assertTrue(GiphyGifTranslator.GIF_PATTERN.matcher("gif wtf").matches());
        assertTrue(GiphyGifTranslator.GIF_PATTERN.matcher("gif a gorilla holding a sandwich").matches());
    }

}