package org.nps.cm.messages.filters.memes;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class ImgflipMemesFilterTest {

    @Test
    public void testPattern() {
        assertTrue(ImgflipMemesFilter.MEMES_PATTERN.matcher("meme surprise koala; oh rly").matches());
    }

}